from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
from conan.tools.files import rename
import os
import subprocess


class mysql(ConanFile):
    name = "mysql"
    description = "mysql"
    topics = ("conan", "mysql", "database")
    url = "https://gitlab.com/terranum-conan/mysql"
    homepage = "https://github.com/mysql"
    license = "GPLv2"
    generators = ["cmake", "cmake_find_package"]
    settings = "os", "arch", "compiler", "build_type"
    # _cmake = None
    version = "5.6.51"
    exports_sources = ["patch/*.patch"]
    options = {"shared": [True, False]}
    default_options = {"shared": False}

    # def configure(self):
    #     if self.settings.os == "Linux":
    #         self.options["wxwidgets"].webview = False # webview control isn't available on linux.

    # def config_options(self):
    #     if self.settings.os == 'Windows':
    #         del self.options.fPIC
    #     if self.settings.os != 'Linux':
    #         self.options.remove('cairo')

    def build_requirements(self):
        if self.settings.os == "Windows" or self.settings.os == "Macos": # Openssl isn't found anymore on latest osx version
            self.build_requires("openssl/1.1.1o") # openssl 3.x isn't found by mysql 5.6.x
    
    def imports(self):
        if self.settings.os == "Windows" or self.settings.os == "Macos":
            self.copy("*", dst="openssl", root_package="openssl")

    def system_requirements(self):
        if self.settings.os == 'Linux' and tools.os_info.is_linux:
            if tools.os_info.with_apt:
                installer = tools.SystemPackageTool()
                packages = []
                packages.append('libssl-dev')
                packages.append('libncurses5-dev')
                for package in packages:
                    installer.install(package)

    def source(self):
        tools.get(**self.conan_data["sources"][self.version], strip_root=True)

    def build(self):
        for patch in self.conan_data.get("patches", {}).get(self.version, []):
            tools.patch(base_path=os.path.join(self.source_folder, patch["base_path"]),
                        patch_file=os.path.join(self.source_folder, patch["patch_file"]))
        
        version_file = os.path.join(self.source_folder, "VERSION") # bug with VERSION file... see : https://bugs.mysql.com/bug.php?id=104126
        if os.path.exists(version_file):
            rename(self, version_file, os.path.join(self.source_folder, "VERSION_BAK"))

        cmake = CMake(self)
        cmake.definitions["WITH_UNIT_TEST"] = "OFF"
        cmake.definitions["WITH_EMBEDDED_SERVER"] = "ON"
        #cmake.definitions["FEATURE_SET"] = "small"
        #cmake.definitions["WITH_INNOBASE"] = "1"
        #cmake.definitions["PLUGIN_INNOBASE"] = "YES"
        if self.settings.os == "Windows" or self.settings.os == "Macos":
            cmake.definitions["WITH_SSL"] = os.path.join(self.build_folder, "openssl")
        cmake.configure()
        cmake.build()

    def package(self):
        # copy headers
        self.copy("*.h", dst="include", src=os.path.join(self.source_folder, "include"))
        self.copy("*.h", dst="include", src=os.path.join(self.build_folder, "include"))
        
        # copy lib or dlls for Windows
        self.copy("*.lib", dst="lib", src=os.path.join(self.build_folder, "libmysqld"), keep_path=False)
        self.copy("*.dll", dst="bin", src=os.path.join(self.build_folder, "libmysqld"), keep_path=False)
        
        # copy lib on OSX
        self.copy("*.dylib", dst="lib", src=os.path.join(self.build_folder, "libmysqld"))
        self.copy("*.a", dst="lib", src=os.path.join(self.build_folder, "libmysqld"))
        #self.copy("*.a", dst="lib", src=self.build_folder, excludes="libmysql/*", keep_path=False)

        # copy plugins
        self.copy("*.so", dst="lib/plugin", src=os.path.join(self.build_folder, "plugin"), keep_path=False)
        
        #copy errmsg file
        self.copy("*.sys", dst="share", src=os.path.join(self.build_folder, "sql", "share"), keep_path=True)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
        if tools.os_info.is_linux:
            self.cpp_info.libs.append("pthread")
            self.cpp_info.libs.append("ssl")
            self.cpp_info.libs.append("crypto")
            self.cpp_info.libs.append("dl")
            self.cpp_info.libs.append("crypt")

