# mysql #

## One step package creation

        conan create . terranum-conan+mysql/stable

> One step package creation isn't working on Windows because we hit this problem : https://jira.mariadb.org/browse/MDEV-22479. The plus sign in "terranum-conan+mysql/stable" leads to the problem. As a workaround, the step by step package creation is working.

## Upload package to gitlab

      conan upload mysql/5.6.51@terranum-conan+mysql/stable --remote=gitlab --all

## Build the debug package

      conan create . terranum-conan+mysql/stable -s build_type=Debug

Don't upload debug package on Gitlab.

## Step by step package creation

1. Get source code

        conan source . --source-folder=_bin/source
2. Create install files

        conan install . --install-folder=_bin/build
3. Build

        conan build . --source-folder=_bin/source --build-folder=_bin/build
4. Create package 

        conan package . --source-folder=_bin/source --build-folder=_bin/build --package-folder=_bin/package
5. Export package

        conan export-pkg . terranum-conan+mysql/stable --source-folder=_bin/source --build-folder=_bin/build
6. Test package
      
        conan test test_package mysql/5.6.51@terranum-conan+mysql/stable





        